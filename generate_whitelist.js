(async function(oG) {
    var whitelist=[];
    var start=Date.now()
    console.log("Started on "+(new Date(start).toString()))
    function pause() {
        return new Promise(e=>{
            setTimeout(function(){e()},1);
        })
    }
    async function addLettersExcept(q,g) {
        var a="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_%+=/\\.@!,$^()[]{}<>|!~\"':;";
        for(var i=0;i<a.length;i++) {
            if(a[i]!=g){ whitelist.push(q+a[i]+"*");parent.console.log(`Adding ${q+a[i]+"*"} to whitelist...`) }
            await pause();
        }
    }

})(3);

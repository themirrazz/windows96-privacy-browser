//!wrt
const {Theme}=w96.ui;
const {StandardWindow,FS}=w96;

var csp=''

class PrivacyBrowser extends WApplication {
  constructor() { super() }
  async main(argv) {
    var mainwnd=this.createWindow({
      title: "Privacy Browser",
      center: true,
      taskbar: true,
      body: `<div style="display: flex;flex-direction:column;width:100%;height:100%;">
        <div class="prvb-tabs" style="display: flex;flex-direction:row;width:100%;">
        </div>
        <div class="prvb-views" style="display: flex;flex-direction:row;width:100%;height:100%;background:white;">
        </div>
      </div>`
    });
    mainwnd.show()
  }
}

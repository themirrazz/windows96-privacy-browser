function generatePaths(length) {
    var chars="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_%+=/\\.@!,$^()[]{}<>|!~\"':;";
    var out=[];
    out.push(...getAllChars(chars));
    if(length>1) {
        var w=generatePaths(length-1);
        w.forEach(d=>{
            getAllChars(chars).forEach(z=>{
                out.push(z+d);
            })
        })
    }
    return out
}

function getAllChars(g) {
    var e=[];
    for(var i=0;i<g.length;i++){e.push(g[i])}
    return e
}

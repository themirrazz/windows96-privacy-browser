# Windows 96 Privacy Browser
A browser dedicated to protecting your privacy, that runs right on Windows 96!

## UI/Usability Features
* **Multi-tabbed** Like Border, this browser has multiple tabs. However, the purpose is NOT to compete against Border.
* **Bookmarks** Save pages for later without manually entering URLs
* **PWA support** Websites that support PWAs can be downloaded and launched in a seperate Window
* **Search** Directly search on your favorite search engine directly in the address bar

## Privacy Features
* **Ad and Tracker Blocking** Blocks common ads and trackers from across the web
* **Content Settings** Control what sites can access and what they can't
* **Disable JavaScript** Prevent websites from running scripts
